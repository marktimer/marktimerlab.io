import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CountdownComponent } from './countdown/countdown.component';
import { CountdownControlComponent } from './countdown-control/countdown-control.component';
import { FormsModule } from '@angular/forms';
import { BatchStepComponent } from './batch-step/batch-step.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

import { NotificationSettingsComponent } from './notification-settings/notification-settings.component';
import { FootnotesComponent } from './footnotes/footnotes.component';
import { InstallBannerComponent } from './install-banner/install-banner.component';

@NgModule({
  declarations: [
    AppComponent,
    CountdownComponent,
    CountdownControlComponent,
    BatchStepComponent,
    NotificationSettingsComponent,
    FootnotesComponent,
    InstallBannerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
  ],
  providers: [
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
