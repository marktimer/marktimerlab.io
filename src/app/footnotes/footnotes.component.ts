import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footnotes',
  templateUrl: './footnotes.component.html',
  styleUrls: ['./footnotes.component.scss']
})
export class FootnotesComponent implements OnInit {

  public showFootnotes = true;

  constructor() { }

  ngOnInit(): void {
  }

}
