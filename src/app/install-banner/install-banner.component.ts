import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-install-banner',
  templateUrl: './install-banner.component.html',
  styleUrls: ['./install-banner.component.scss']
})
export class InstallBannerComponent implements OnInit {

  private installEvent: any;
  public showBanner = false;

  @HostListener('window:beforeinstallprompt', ['$event'])
  onbeforeinstallprompt(e: any): void {
    console.log(e);
    // e.preventDefault();
    if (!this.getVisited()) {
      this.installEvent = e;
      this.showBanner = true;
    }
  }

  constructor() { }

  ngOnInit(): void {
  }

  getVisited(): boolean {
    const instPr = localStorage.getItem('marktimer.install-prompt');
    return Boolean(instPr).valueOf();
  }

  setVisited(): void {
    localStorage.setItem('marktimer.install-prompt', 'true');
  }

  hideBanner(): void {
    this.setVisited();
    this.showBanner = false;
  }

  install(): void {
    this.hideBanner();
    this.installEvent.prompt();
    this.installEvent.userChoice.then((choice: any) => {
      if (choice.outcome !== 'accepted') {
        // No thanks -> do not ask again
        this.setVisited();
      }
      this.installEvent = null;
    });
  }

}
