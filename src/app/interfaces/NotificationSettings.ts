export interface NotificationSettings {
  notification: 'notification' | 'beep' | 'audio';
  name: string;
  audioSrc?: string;
}
