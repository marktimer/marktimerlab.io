import { BatchStep } from './BatchStep';

export interface PredefinedBatch {
  'name': string;
  'steps': BatchStep[];
}
