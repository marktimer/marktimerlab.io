export interface BatchStep {
  hours?: number;
  minutes?: number;
  seconds?: number;
  comment?: string;
}
