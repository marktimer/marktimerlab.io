import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NotificationSettings } from '../interfaces/NotificationSettings';

@Component({
  selector: 'app-notification-settings',
  templateUrl: './notification-settings.component.html',
  styleUrls: ['./notification-settings.component.scss']
})
export class NotificationSettingsComponent implements OnInit {

  public options: NotificationSettings[] = [
    {notification: 'beep', name: 'Ugly Beep'},
    {notification: 'notification', name: 'Desktop Notification'},
    {notification: 'audio' , audioSrc: '/assets/cuckooclock-publicdomain.mp3', name: 'Cuckoo Clock'},
  ];

  @Output() finished: EventEmitter<NotificationSettings> = new EventEmitter();
  @Input() inSettings: NotificationSettings = this.options[0];

  public settings: NotificationSettings = this.options[0];

  constructor() { }

  ngOnInit(): void {
    const itsthere = this.options.find(n => n.name === this.inSettings.name);
    this.settings = itsthere ? itsthere : this.options[0];
  }

  public closeSettings(): void {
    this.finished.emit(this.settings);
  }

}
