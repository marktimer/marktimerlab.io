import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchStepComponent } from './batch-step.component';

describe('BatchStepComponent', () => {
  let component: BatchStepComponent;
  let fixture: ComponentFixture<BatchStepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BatchStepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
