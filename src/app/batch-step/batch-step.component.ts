import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Duration } from 'luxon';
import { BatchStep } from '../interfaces/BatchStep';

@Component({
  selector: 'app-batch-step',
  templateUrl: './batch-step.component.html',
  styleUrls: ['./batch-step.component.scss']
})
export class BatchStepComponent implements OnInit {

  @Input() durationObject: BatchStep = { minutes: 1 };
  @Input() index = 0;
  @Output() startStep = new EventEmitter<number>();
  @Output() deleteStep = new EventEmitter<number>();
  @Output() newStepAfter = new EventEmitter<{ step: BatchStep, index: number }>();

  public h = '00';
  public min = '00';
  public s = '00';
  public showTime = '00';
  public showForm = false;

  constructor() { }

  ngOnInit(): void {
    this.showTime = Duration.fromObject(this.pickFromObject(this.durationObject, 'hours', 'minutes', 'seconds')).toFormat('hh:mm:ss');
  }

  emitStartStep(): void {
    this.startStep.emit(this.index);
  }

  emitDeleteStep(): void {
    this.deleteStep.emit(this.index);
  }

  addStep(form: NgForm): void {
    // Delete empty object properties. Thanks StackOverflow ;)
    const step = Object.keys(form.value)
      .filter(k => form.value[k] !== '')
      .reduce((a, k) => ({ ...a, [k]: form.value[k] }), {});
    this.newStepAfter.emit({ step, index: this.index });
    this.showForm = false;
  }

  // https://stackoverflow.com/questions/25553910/one-liner-to-take-some-properties-from-object-in-es-6
  pickFromObject(o: any, ...fields: any[]): object {
    return fields.reduce((a, x) => {
      if (o.hasOwnProperty(x)){
        a[x] = o[x];
      }
      return a;
    }, {});
  }
}
