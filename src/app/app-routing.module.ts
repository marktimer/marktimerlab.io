import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountdownControlComponent } from './countdown-control/countdown-control.component';

const routes: Routes = [
  { path: '', component: CountdownControlComponent },
  { path: ':cloudUrl', component: CountdownControlComponent },
  // default route
  // { path: '', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
