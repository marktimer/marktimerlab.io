import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Subscription, interval } from 'rxjs';
import { Duration } from 'luxon';
import { Title } from '@angular/platform-browser';
import { BatchStep } from '../interfaces/BatchStep';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit, OnDestroy, OnChanges {

  @Input() durationObject: BatchStep = { minutes: 1 };
  @Input() restart = false;
  @Output() countZero = new EventEmitter();
  @Output() countOne = new EventEmitter();
  @Output() countTwo = new EventEmitter();
  @Output() countThree = new EventEmitter();

  private subscription = new Subscription();
  private timeDifference = Duration.fromMillis(0);
  public showTime = '00:00:00';
  public comment = '';

  constructor(private titleService: Title) { }

  public setTitle(newTitle: string): void {
    this.titleService.setTitle(newTitle);
  }

  ngOnInit(): void {
    this.setupTimer();
    if (typeof Worker !== 'undefined') {
      // Create a new
      const worker = new Worker('./countdown.worker', { type: 'module' });
      worker.onmessage = ({ data }) => {
        this.handleTick();
      };
      worker.postMessage('start');
    } else {
      // Web Workers are not supported in this environment.
      // You should add a fallback so that your program still executes correctly.
      this.subscription = interval(100).subscribe(() => {
        this.handleTick();
      });
    }
  }

  handleTick(): void {
    this.timeDifference = this.timeDifference.minus(100).normalize();
    this.showTime = this.timeDifference.toFormat('hh:mm:ss');
    this.setTitle(this.showTime + '|' + this.comment + '|>Marktimer');
    if (this.timeDifference.toMillis() === 0) {
      this.countZero.emit();
      console.log('Zero');
    }
    if (this.timeDifference.toMillis() === 1000) {
      this.countOne.emit();
      console.log('One');
    }
    if (this.timeDifference.toMillis() === 2000) {
      this.countTwo.emit();
      console.log('Two');
    }
    if (this.timeDifference.toMillis() === 3000) {
      this.countThree.emit();
      console.log('Three');
    }
  }

  ngOnChanges(): void {
    this.setupTimer();
  }

  setupTimer(): void {
    this.comment = this.durationObject.comment || '';
    this.timeDifference = Duration.fromObject(this.pickFromObject(this.durationObject, 'hours', 'minutes', 'seconds'));
    // Just to make sure...
    this.timeDifference = this.timeDifference.set({ milliseconds: 0 });
  }

  // https://stackoverflow.com/questions/25553910/one-liner-to-take-some-properties-from-object-in-es-6
  pickFromObject(o: any, ...fields: any[]): object {
    return fields.reduce((a, x) => {
      if (o.hasOwnProperty(x)) {
        a[x] = o[x];
      }
      return a;
    }, {});
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
