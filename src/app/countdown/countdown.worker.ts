/// <reference lib="webworker" />

let interval: number;

addEventListener('message', ({ data }) => {
  const response = `worker response to ${data}`;
  switch (data) {
    case 'start':
      interval = setInterval(() => postMessage('tick'), 100);
      break;
    case 'stop':
      clearInterval(interval);
      break;

  }
});
