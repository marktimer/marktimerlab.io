import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountdownControlComponent } from './countdown-control.component';

describe('CountdownControlComponent', () => {
  let component: CountdownControlComponent;
  let fixture: ComponentFixture<CountdownControlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountdownControlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountdownControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
