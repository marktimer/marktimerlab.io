import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BatchStep } from '../interfaces/BatchStep';
import { PredefinedBatch } from '../interfaces/PredefinedBatch';

import { ActivatedRoute } from '@angular/router';
import { NotificationSettings } from '../interfaces/NotificationSettings';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-countdown-control',
  templateUrl: './countdown-control.component.html',
  styleUrls: ['./countdown-control.component.scss']
})
export class CountdownControlComponent implements OnInit {

  public countDownStep: BatchStep = { minutes: 1 };
  public countDownBatch: BatchStep[] = [];
  private currBatchStep = 0;
  public modeRepeat = false;
  public restartToggle = false;
  public showOneShot = false;
  public showSaveLocal = false;
  public showSaveCloud = false;
  public batchUrl = '';
  public cloudSaving = false;
  private notification: Notification;
  public showNotificationSettings = false;

  public notificationSettings: NotificationSettings = {notification: 'beep', name: 'Ugly Beep'};
  public predefinedBatches: PredefinedBatch[] = [];

  constructor(private route: ActivatedRoute) {
    // Workaround "INITIALIZE!!"
    this.notification = new Notification('');
    this.notification.close();
  }

  ngOnInit(): void {
    const cloudUrl = this.route.snapshot.params.cloudUrl;
    this.listLocalBatches();
    if (cloudUrl) {
      this.loadCloud(cloudUrl);
    }
  }

  startCountdown(form: NgForm): void {
    this.showOneShot = false;
    // Delete empty object properties. Thanks StackOverflow ;)
    this.countDownStep = Object.keys(form.value)
      .filter(k => form.value[k] !== '')
      .reduce((a, k) => ({ ...a, [k]: form.value[k] }), {});

    if (this.countDownBatch.length <= 1) {
      this.countDownBatch[0] = this.countDownStep;
      this.currBatchStep = 0;
    }
  }

  startPredefinedBatch(batchNo: number): void {
    if (batchNo < this.predefinedBatches.length) {
      this.countDownBatch = this.predefinedBatches[batchNo].steps;
      this.countDownStep = this.countDownBatch[0];
    }
  }

  startBatchStep(index: number): void {
    this.countDownStep = this.countDownBatch[index];
    this.currBatchStep = index;
    this.restartToggle = !this.restartToggle;
  }

  deleteBatchStep(index: number): void {
    this.countDownBatch.splice(index, 1);
  }

  insertBatchStep(bs: { step: BatchStep, index: number }): void {
    this.countDownBatch.splice(bs.index + 1, 0, bs.step);
  }

  toggleRepeat(): void {
    this.modeRepeat = !this.modeRepeat;
  }

  timeUp(): void {
    this.currBatchStep++;
    if (this.currBatchStep >= this.countDownBatch.length) {
      if (this.modeRepeat) {
        this.currBatchStep = 0;
      }
    }
    if (this.currBatchStep < this.countDownBatch.length) {
      this.startBatchStep(this.currBatchStep);
    }
  }

  notify(secondsLeft: number): void {
    switch (this.notificationSettings.notification) {
      case 'notification':
        this.createNotification(secondsLeft);
        break;
      case 'beep':
        this.createUglyBeep(secondsLeft ? 0.5 : 1.5);
        break;
      case 'audio':
        if (this.notificationSettings.audioSrc) {
          this.playAudio(this.notificationSettings.audioSrc);
        } else {
          this.createUglyBeep(secondsLeft ? 0.5 : 1.5);
        }
        break;
    }
    if (secondsLeft === 0) {
      this.timeUp();
    }
  }

  private createNotification(secondsLeft: number): void {
    this.notification.close();
    const text = secondsLeft ? secondsLeft.toString() + 's!' : 'OVER!';
    this.notification = new Notification('Marktimer', { body: this.currBatchStep + text});

  }

  public createUglyBeep(duration: number): void {
    const context = new AudioContext() || (window as any).webkitAudioContext();
    const o = context.createOscillator();
    const startTime = context.currentTime;
    o.connect(context.destination);
    o.type = 'sine';
    o.start(startTime);
    o.stop(startTime + duration);
  }

  public playAudio(src: string): void {
    new Audio(src).play();
  }

  public saveLocal(form: NgForm): void {
    if (form.value.name) {
      localStorage.setItem(form.value.name, JSON.stringify(this.countDownBatch));
    }
    this.listLocalBatches();
    this.showSaveLocal = false;
  }

  public deleteLocal(name: string): void {
    localStorage.removeItem(name);
    this.listLocalBatches();
    this.showSaveLocal = false;
  }

  private listLocalBatches(): void {
    this.initPredefinedBatches();
    for (let i = 0, len = localStorage.length; i < len; i++) {
      try {
        const key = localStorage.key(i);
        const item = localStorage.getItem(key || 'nonexistantkeydummy');
        if (key && item) {
          const steps = JSON.parse(item);
          if (steps[0] && (
              steps[0].hasOwnProperty('hours') ||
              steps[0].hasOwnProperty('minutes') ||
              steps[0].hasOwnProperty('seconds') ||
              steps[0].hasOwnProperty('comment'))
             ){
            const batch: PredefinedBatch = { name: key, steps};
            this.predefinedBatches.push(batch);
          }
        }
      } catch (error) {
        console.log(error.message);
      }
    }
  }

  public async saveCloud(form: NgForm): Promise<void> {
    this.cloudSaving = true;
    const Parse = await import('parse');
    Parse.initialize(environment.PARSE_APP_ID, environment.PARSE_JS_KEY);
    Parse.CoreManager.set('SERVER_URL', environment.serverURL);
    // (Parse as any).serverURL = environment.serverURL;
    if (form.value.name) {
      const CloudBatch = Parse.Object.extend('cloudBatch');
      const cloudBatch = new CloudBatch();
      cloudBatch.set('name', form.value.name);
      cloudBatch.set('batch', JSON.stringify(this.countDownBatch));
      try {
        const result = await cloudBatch.save();
        console.log('Parse: Done, ObjectID: ' + result.id);
        this.batchUrl = 'marktimer.gitlab.io/' + result.id;
      } catch (error) {
        console.log('Parse: Error: ' + error.message);
      }
    }
    this.cloudSaving = false;
    this.showSaveCloud = false;
  }

  public async loadCloud(id: string): Promise<void> {
    const Parse = await import('parse');
    Parse.initialize(environment.PARSE_APP_ID, environment.PARSE_JS_KEY);
    Parse.CoreManager.set('SERVER_URL', environment.serverURL);
    // (Parse as any).serverURL = environment.serverURL;
    try {
      const CloudBatch = Parse.Object.extend('cloudBatch');
      const query = new Parse.Query(CloudBatch);
      query.equalTo('objectId', id);
      query.first().then(data => {
        if (data) {
          const batchnum = this.predefinedBatches.push({ name: data.get('name'), steps: JSON.parse(data.get('batch'))});
          this.startPredefinedBatch(batchnum - 1);
          data.set('lastRead', new Date().toString());
          data.save().then(r => console.log(r));
        }
      });
    } catch (error) {
      console.log('Parse: Error: ' + error.message);
    }
  }

  private initPredefinedBatches(): void {
    this.predefinedBatches.length = 0;
    this.predefinedBatches.push({ name: 'Work-Break-Flow',
                                  steps: [
                                         { minutes: 45, comment: 'FocusWork' },
                                         { minutes: 5, comment: 'E-Mail' },
                                         { minutes: 5, comment: 'Move!' }
                                         ]
                                });
    this.predefinedBatches.push({ name: 'Pomodoro',
                                  steps: [
                                         { minutes: 25, comment: 'Pomodoro 1' },
                                         { minutes: 5, comment: 'Short Break 1' },
                                         { minutes: 25, comment: 'Pomodoro 2' },
                                         { minutes: 5, comment: 'Short Break 2' },
                                         { minutes: 25, comment: 'Pomodoro 3' },
                                         { minutes: 5, comment: 'Short Break 3' },
                                         { minutes: 25, comment: 'Pomodoro 4' },
                                         { minutes: 15, comment: 'Long Break!' },
                                         ]
                                });
  }

  setNotificationSettings(settings: NotificationSettings): void {
    this.showNotificationSettings = false;
    this.notificationSettings = settings;
    if (settings.notification === 'notification') {
      Notification.requestPermission().then(permission => {
        if (permission === 'denied' || permission === 'default') {
          this.notificationSettings = {notification: 'beep', name: 'Ugly Beep'};
        }
      });
    }
  }
}
