# Marktimer
https://marktimer.gitlab.io  
This is just another timer app, heavily inspired by marinaratimer, pomotroid and others.

## Why another one? There are literally thousands!
Yes, true. But I did not find what I wanted, so after long searching and fiddling I finally decided to make my own (you know these words, don't you? :wink:) marinaratimer was very close, but for me it did not work reliably (did hang sometimes) and there was no option to keep counting into negative after the time was up. So, if I was late, I never knew how much - but for some things you should know...

## Help
You are very much invited to help! This is just html + css + angular, so if you know any one of those three I am happy to receive your MR :smile: :thumbsup:  
Or just your ideas how to improve marktimer in an issue. :point_left:

I would also be happy to read about how you use marktimer and how it helps you! :gift: :tada:

And, of course, you can make me happy and motivated to continue the work on marktimer if you [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/Q5Q84BUVY)
